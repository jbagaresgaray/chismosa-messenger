'use strict';

angular.module('todo', ['ionic', 'firebase', 'angularMoment'])
    .constant('msdElasticConfig', {
        append: ''
    })
    .constant('FIREBASE_ROOT', 'https://burning-fire-4853.firebaseio.com/')
    .run(function($ionicPlatform, $rootScope, $state, Auth, $ionicLoading) {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            ionic.Platform.fullScreen();

            $rootScope.displayName = null;
            $rootScope.user = [];

            Auth.$onAuth(function(authData) {
                if (authData) {
                    console.log('Logged in as:', authData.uid);
                } else {
                    console.log('Logged out');
                    $ionicLoading.hide();
                    $state.go('login');
                }
            });

            $rootScope.logout = function() {
                console.log('Logging out from the app');
                window.localStorage.removeItem('users');
                $ionicLoading.show({
                    template: 'Logging Out...'
                });
                Auth.$unauth();
            }


            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                if (error === 'AUTH_REQUIRED') {
                    $state.go('login');
                }
            });
        });
    });
