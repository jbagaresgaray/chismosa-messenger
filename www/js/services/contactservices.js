'use strict';

angular.module('todo')
    .factory('Contacts', function($firebase, $firebaseArray,FIREBASE_ROOT) {
        var ref = new Firebase(FIREBASE_ROOT);
        var contacts = $firebaseArray(ref.child('contacts'));

        return {
            all: function() {
                return contacts;
            },
            get: function(contactId) {
                console.log('contactId: ', contactId);
                return contacts.$getRecord(contactId);
            },
            create: function(contact, email) {
                console.log("create contacts name :" + contact.name + " & message is " + email);
                if (contact && email) {
                    var contact = {
                        name: contact.name,
                        number: contact.number,
                        email: email,
                        createdAt: Firebase.ServerValue.TIMESTAMP
                    };
                    chats.$add(chatMessage).then(function(data) {
                        console.log("contacts added");
                    });
                }
            }
        }
    });
