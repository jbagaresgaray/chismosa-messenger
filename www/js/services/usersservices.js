'use strict';

angular.module('todo')
    .factory('Users', function($firebase, $firebaseArray, $firebaseObject,FIREBASE_ROOT) {
        var ref = new Firebase(FIREBASE_ROOT);
        var users = $firebaseObject(ref.child('contacts'));

        return {
            all: function() {
                return users;
            },
            get: function(contactId) {
                return users.$getRecord(contactId);
            }
        }
    });
