'use strict';

angular.module('todo')
    .factory('Rooms', function($firebase, $firebaseArray, $firebaseObject,FIREBASE_ROOT) {
        var ref = new Firebase(FIREBASE_ROOT);
        var rooms = $firebaseObject(ref.child('rooms'));

        return {
            all: function() {
                return rooms;
            },
            get: function(contactId) {
                return rooms.$getRecord(roomId);
            }
        }
    });
