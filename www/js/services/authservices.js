'use strict';

angular.module('todo')
    .factory('Auth', function($firebaseAuth, $rootScope,FIREBASE_ROOT) {
        var ref = new Firebase(FIREBASE_ROOT);
        return $firebaseAuth(ref);
    });
