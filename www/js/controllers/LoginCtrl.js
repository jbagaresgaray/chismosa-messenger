'use strict';

angular.module('todo')
    .controller('LoginCtrl', function($scope, $ionicModal, $ionicPopup, $state, $firebaseAuth, $ionicLoading, $rootScope, $ionicPlatform, FIREBASE_ROOT, Auth) {
        console.log('LoginCtrl');
        $scope.user = {};

        var ref = new Firebase(FIREBASE_ROOT);
        var auth = $firebaseAuth(ref);
        $ionicModal.fromTemplateUrl('templates/signup.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        Auth.$onAuth(function(authData) {
            if (authData === null) {
                console.log('Not logged in yet');
            } else {
                console.log('Logged in as', authData.uid);
            }
            // This will display the user's name in our view
            $scope.authData = authData;
        });

        $scope.createUser = function(user) {
            console.log("Create User Function called");
            if (user && user.email && user.password && user.displayname) {
                $ionicLoading.show({
                    template: 'Signing Up...'
                });

                auth.$createUser({
                    email: user.email,
                    password: user.password
                }).then(function(userData) {
                    $ionicPopup.alert({
                        title: 'Success',
                        template: 'User created successfully!'
                    });

                    ref.child("users").child(userData.uid).set({
                        email: user.email,
                        displayName: user.displayname,
                    });

                    $ionicLoading.hide();
                    $scope.modal.hide();
                }).catch(function(error) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Error: ' + error
                    });
                    $ionicLoading.hide();
                });
            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Please fill all details'
                });
            }
        };

        $scope.signIn = function(user) {

            if (user && user.email && user.pwdForLogin) {
                $ionicLoading.show({
                    template: 'Signing In...'
                });
                auth.$authWithPassword({
                    email: user.email,
                    password: user.pwdForLogin
                }).then(function(authData) {
                    console.log("Logged in as:" + authData.uid);
                    ref.child("users").child(authData.uid).once('value', function(snapshot) {
                        var val = snapshot.val();
                        // To Update AngularJS $scope either use $apply or $timeout
                        $scope.$apply(function() {
                            $rootScope.displayName = val;
                            window.localStorage['users'] = JSON.stringify(val);
                        });
                    });
                    $ionicLoading.hide();
                    $state.go('tab.home');
                }).catch(function(error) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Authentication failed: ' + error.message
                    });
                    $ionicLoading.hide();
                });
            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Please enter email and password both'
                });
            }
        };

        $scope.loginSocial = function(authMethod) {
            Auth.$authWithOAuthRedirect(authMethod).then(function(authData) {}).catch(function(error) {
                if (error.code === 'TRANSPORT_UNAVAILABLE') {
                    Auth.$authWithOAuthPopup(authMethod).then(function(authData) {});
                } else {
                    console.log(error);
                }
            });
        };

    });
