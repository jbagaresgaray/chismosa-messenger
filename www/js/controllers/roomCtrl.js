'use strict';

angular.module('todo')
    .controller('RoomsCtrl', function($scope, Rooms, Chats, $state) {
        //console.log("Rooms Controller initialized");
        $scope.rooms = Rooms.all();
        console.log(' Rooms.all(); ', Rooms.all());

        $scope.openChatRoom = function(roomId) {
            $state.go('tab.chat', {
                roomId: roomId
            });
        }

    });
