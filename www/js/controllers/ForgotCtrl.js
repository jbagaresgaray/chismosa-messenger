'use strict';

angular.module('todo')
    .controller('ForgotCtrl', function($scope, $state, $location, $ionicSlideBoxDelegate) {
        // Called to navigate to the main app
        console.log('ForgotCtrl');
        $scope.doSignUp = function() {
            $location.path('/signup');
        }
        $scope.doLogin = function() {
            $location.path('/login');
        }
    });
