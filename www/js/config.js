'use strict';

angular.module('todo')
	.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: "templates/login.html",
            controller: 'LoginCtrl',
            resolve: {
                "currentAuth": ["Auth",
                    function(Auth) {
                        return Auth.$waitForAuth();
                    }
                ]
            }
        })
        .state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html",
            controller: 'AppCtrl',
            resolve: {
                "currentAuth": ["Auth",
                    function(Auth) {
                        return Auth.$requireAuth();
                    }
                ]
            }
        })
        .state('tab.home', {
            url: "/home",
            views: {
                'tab-home': {
                    templateUrl: 'templates/tab-home.html',
                    // controller: 'RoomsCtrl'
                }
            }
        })
        .state('tab.contacts', {
            url: '/contacts',
            views: {
                'tab-contacts': {
                    templateUrl: 'templates/tab-contacts.html',
                    controller: 'ContactsCtrl'
                }
            }
        })
        .state('tab.chatdetail', {
            url: '/chatdetail/:id',
            views: {
                'tab-chat': {
                    templateUrl: 'templates/tab-chatDetail.html',
                    controller: 'ChatDetailCtrl'
                }
            }
        })
        .state('tab.chat', {
            url: '/chat',
            views: {
                'tab-chat': {
                    templateUrl: 'templates/tab-message.html',
                    controller: 'ChatCtrl'
                }
            }
        })
        .state('tab.profile', {
            url: "/profile",
            views: {
                'tab-profile': {
                    templateUrl: "templates/tab-profile.html",
                    // controller: 'ProfileCtrl'
                }
            }
        })
        /*.state('forgot', {
            url: "/forgot",
            templateUrl: "templates/forgot.html",
            controller: 'ForgotCtrl'
        })
        .state('signup', {
            url: "/signup",
            templateUrl: "templates/signup.html",
            controller: 'SignupCtrl'
        })
        .state('intro', {
            url: "/intro",
            templateUrl: "templates/intro.html",
            controller: 'IntroCtrl'
        })
        .state('app.home', {
            url: "/home",
            views: {
                'home-tab': {
                    templateUrl: "templates/messages.html",
                    controller: 'MsgCtrl'
                }
            }
        })
        .state('app.search', {
            url: "/search",
            views: {
                'menuContent': {
                    templateUrl: "templates/search.html",
                    controller: 'SearchCtrl'
                }
            }
        })
        .state('app.message-detail', {
            url: "/messages/:id",
            views: {
                'menuContent': {
                    templateUrl: "templates/messageDetail.html",
                    controller: 'MsgCtrl'
                }
            }
        });*/

    $urlRouterProvider.otherwise('/login');
})